# Monipor - The IP address monitor

Script that monitors the IP address of a host and sends notifications when it changes.

## Installation

Install the dependencies (as root because we want to be able to use it as root. Just doing `sudo pip3 ...` wasn't enough.):

```bash
sudo su
pip3 install -r requirements.txt
exit
```

Download the script, mark it as executable and move it to a location that should be in your $PATH:

```bash
curl https://gitlab.com/thgoebel/monipor/raw/master/monipor > monipor
chmod +x monipor
mv monipor /usr/local/bin
```

Create the config file, make sure it is owned by and only accessible to root, and fill it out:

```bash
sudo cp monipor.example.toml /root/.monipor.toml
sudo chown root:root /root/.monipor.toml
sudo chmod 600 /root/.monipor.toml
sudo vim /root/.monipor.toml
```

Start using monipor!

```bash
sudo monipor --help
sudo monipor --summary
```

## Further steps

You can view monipor's logs at `/var/log/monipor.log`.

Let's set up two cronjobs (for the root user) that check the IP every five minutes and send
a weekly summary to verify monipor is still working:

```bash
sudo crontab -e
```

Now append the following lines:

```
@weekly /usr/local/bin/monipor -s > /dev/null 2>&1
*/5 * * * * /usr/local/bin/monipor > /dev/null 2>&1
```

Three things to notice:

1. No sudo because it is root's crontab.
2. Explicit path to monipor. Better safe than sorry.
3. Redirect any output to /dev/null (both stdout and stderr). Relevant messages are logged to `/var/log/monipor.log` anyway. But you can change this to some temporary file for troubleshooting.

## Releases and Changelog

There are currently no dedicated releases.
The commit history is the changelog.

If you do use monipor in an automated setup it is therefore recommended to pin a specific commit.
In the URL given to `curl` in the instructions above, simply replace `master` with the desired commit hash.

## Security

To send out messages, monipor stores your email password or Threema Gateway private key in a file.
How are they kept safe?

First, monipor will also complain if it is not run as root. Thus all data written by monipor will be
written as the root user.
Second, whenever monipor writes data to file it changes the permissions on that file to ensure that only 
its owner can read and write to it.

In combination, this should make sure that your secrets are as well protected as your root password is.
So be careful as to who has root privileges on your machine! Also, only manually change monipor's files'
permissions if you know what you are doing.

## About

Author: Thore Goebel

License: GNU General Public License v3 or later ([copy](https://gitlab.com/thgoebel/monipor/blob/master/LICENSE))
